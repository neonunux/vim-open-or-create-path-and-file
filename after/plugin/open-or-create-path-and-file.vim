" openOrCreateFileAndPath.vim
"
" Vim global plugin for open or create file & path
" PluginName: vim-open-or-create-file-and-path
" Last Change:  2019 Aug 29
" Maintainer:   Régis LELOUP <regis.leloup@colombbus.org>
" Version:  1.0.0
" License:  GNU GPLv3.0
" available for unix/linux
"
" 1. Open the file under the cursor: <leader>e
"   - if file exists, file is open
"   - if file doesn't exist, parent dir are created if needed and also file
"   TODO:
"       - supports includeexpr
"
" test cases:
" /home/taminio/existfilename.ext
" ~/existfilename.ext
" ~/test/newdir/newfile.ext
" ~/test/newdir/newfile " with set suffixesadd=.ext
" ~/regis
"

if &cp || exists('g:open_or_create_file')
    finish
endif

let g:create_path_file= 1
let s:save_cpo = &cpo
set cpo&vim

function! s:openOrCreateFile()
    let s:bundle = expand('<cfile>:p')
    let path = expand('<cfile>:p:h')

    let suffixes = split(&suffixesadd, ',')
    let isExists = 0
    for s in suffixes
        if empty(glob(s:bundle . s))
            let isExists = 1
        endif
    endfor

    if !isExists
        let answer = input("file doesn't exists; create file ?")

        if answer ==? 'y'
            if !isdirectory(path)
                echomsg path . ' created'
                "netrw_localmkdiropts("p")
                "netrw_localmkdir(path)
                :call mkdir(path, 'p')
            endif
        endif
    endif
    ":edit <cfile> " create or edit now path is available
    :norm! gf
endfunction

command -nargs=0 OpenOrCreateFile call s:openOrCreateFile()

if !exists('g:open_or_create_path_file_custom_keymap')
    nnoremap <leader>e :OpenOrCreateFile<CR>
endif

let &cpo = s:save_cpo

