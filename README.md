vim-open-or-create-path-and-file
===========

Open a file under cursor but can create an nonexistant file

NOTE: File can be opened with `gf` as usual.

## Commands
* `OpenOrCreateFile`: open or create path and file under cursor, default key map `<leader>e`

## Config

* `g:open_or_create_path_file_custom_keymap`. This option is used to config the key mappings. If you want to custom the key mappings, set the option to `1`. The default value is `0`

## Change Log
* 2019-08-19
    * Add command `OpenOrCreateFile`
